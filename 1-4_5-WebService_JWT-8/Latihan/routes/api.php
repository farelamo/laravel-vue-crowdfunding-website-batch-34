<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\Articles\ArticleController;
use App\Http\Controllers\Auth\LogoutController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('/login', [LoginController::class, 'login']);
Route::post('/register', [RegisterController::class, 'register']);

Route::middleware('auth:api')->group(function(){
    Route::controller(ArticleController::class)->group(function(){
        Route::get('/get-article', 'index');
        Route::get('/get-article/{article}', 'show');
        Route::post('/create-article', 'store');
        Route::patch('/update-article/{article}', 'update');
        Route::delete('/delete-article/{article}', 'destroy');
    });
    Route::get('/user', [UserController::class, 'getName']);
    Route::post('/logout', [LogoutController::class, 'logout']);
});
