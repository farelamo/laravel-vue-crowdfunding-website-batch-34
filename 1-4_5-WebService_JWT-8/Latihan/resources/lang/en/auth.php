<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'password' => 'The provided password is incorrect.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'name' => [
        'required' => 'name must to be filled',
        'max' => 'maximum name is 50 character ! !'
    ],
    'username' => [
        'required' => 'username must to be filled',
        'max' => 'maximum username is 10 character ! !'
    ],
    'email' => [
        'required' => 'email must to be filled',
        'email' => 'must be type of an email',
        'unique' => 'email has already been taken ! !'
    ],
    'password' => [
        'required' => 'password must to be filled',
        'max' => 'minimum password is 6 character ! !'
    ],

];
