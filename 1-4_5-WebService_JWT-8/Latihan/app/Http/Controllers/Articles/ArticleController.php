<?php

namespace App\Http\Controllers\Articles;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Articles\Article;
use App\Http\Requests\ArticleRequest;
use App\Http\Resources\ArticleResources;
use App\Http\Resources\ArticleCollection;

class ArticleController extends Controller
{
    public function index()
    {
        $articles = Article::all();
        return new ArticleCollection($articles); //tdk bisa nampilkan custom kek resources (published => blablabla)
        // return ArticleResources::collection($articles); //tdk bisa method with status => success
    }

    public function store(ArticleRequest $request)
    {
        $article = auth()->user()->articles()->create([
            'title' => $request->title,
            'slug' => \Str::slug($request->title),
            'body' => $request->body,
            'subject_id' => $request->subject_id
        ]);

        return $article;
    }

    public function show(Article $article)
    {
        return new ArticleResources($article);
    }

    public function update(ArticleRequest $request, Article $article)
    {
        $article->update([
            'title' => $request->title,
            'slug' => \Str::slug($request->title),
            'body' => $request->body,
            'subject_id' => $request->subject_id
        ]);
        return new ArticleResources($article);
    }

    public function destroy(Article $article)
    {
        $article->delete();
        return response()->json([
            'status' => 'success',
            'message' => 'data was successfully deleted'
        ]);
    }
}
