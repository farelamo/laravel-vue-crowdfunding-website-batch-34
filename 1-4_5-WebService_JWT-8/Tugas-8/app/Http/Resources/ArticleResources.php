<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ArticleResources extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        //all data
        // return parent::toArray($request);

        //custom data
        return [
            'status' => 'success',
            'data' => $this->resource
            // 'title' => $this->title,
            // 'published' => $this->created_at->format('d F Y'),
            // 'Long time published' => $this->created_at->diffForHumans(),
            // 'subject' => $this->subject,
            // 'user' => $this->user
        ];
    }

    // public function with($request){
    //     return ['status' => 'success'];
    // }
}
