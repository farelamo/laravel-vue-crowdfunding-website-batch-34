<?php

namespace App\Models\Articles;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\useUuid;

class Subject extends Model
{
    use HasFactory, useUuid;
    
    public function articles(){
        return $this->hasMany(Article::class);
    }
}
