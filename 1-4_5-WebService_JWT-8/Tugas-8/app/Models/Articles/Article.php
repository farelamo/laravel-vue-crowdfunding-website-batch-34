<?php

namespace App\Models\Articles;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\useUuid;
use App\Models\User;

class Article extends Model
{
    use HasFactory, useUuid;

    protected $fillable = ['title', 'slug', 'body', 'subject_id'];
    protected $with = ['subject', 'user'];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function subject(){
        return $this->belongsTo(Subject::class);
    }

    public function getRouteKeyName(){
        return 'slug';
    }
}