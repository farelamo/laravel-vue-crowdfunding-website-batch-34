export const LikeComponent = {
    template: `
        <div class="card">
            <strong>Saya suka : </strong>
            <ul>
                <li><p>Makan Nasi Goreng</p></li>
                <li><p>Bermain Catur</p></li>
                <li><p>Olahraga Kardio</p></li>
            </ul>
        </div>
    `
}