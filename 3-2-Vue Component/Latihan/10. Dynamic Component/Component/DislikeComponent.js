export const DislikeComponent = {
    template: `
        <div class="card">
            <strong>Saya tidak suka : </strong>
            <ul>
                <li><p>Minum Bir</p></li>
                <li><p>Pertikaian</p></li>
                <li><p>Olahraga Berlebihan</p></li>
            </ul>
        </div>
    `
}