// Implementasi pada component local
export var componentBlog = { //Child Component
//template harus dibungkus dengan div / parent node
template: `
    <div>
        <h3>{{ blog.title }} <button @click="$emit('anjay', blog.title)">Pilih</button></h3>
        <p>{{ blog.content }}</p>
    </div>
`,
data(){
    return {
        pesan: 'Ini data dari component lokal A'
    }
},
//transfer data pakai props, props == child component
props: ['blog'] //data child
}