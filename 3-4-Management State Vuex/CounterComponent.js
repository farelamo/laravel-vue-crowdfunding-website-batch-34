export const CounterComponent = {
    template: `
        <div>
            <p>State pada Counter Component {{ counter }}</p>
        </div>
    `,
    computed: {
        counter(){
            return this.$store.getters.counter
        }
    }
}