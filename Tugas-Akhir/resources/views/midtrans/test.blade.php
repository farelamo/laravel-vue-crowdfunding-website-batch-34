<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Test Checkout</title>
</head>
<body>
    <div id="app">
        <button @click="handlePayButton">bayar</button>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue-resource@1.5.1"></script>
    <script src="https://app.sandbox.midtrans.com/snap/snap.js" data-client-key="SB-Mid-client-JvYIBJFDzvY86JZX"></script>
    <script>
        var vm = new Vue({
            el: '#app',
            data: function(){
                return {
                    data_midtrans: {
                        'transaction_details' : {
                            'order_id': "f8e33bbe-bf35-4ad4-927c-719657bbb9bd-qjv82",
                            'gross_amount': 20000
                        },
                        'customer_details': {
                            'first_name': 'Fariz',
                            'last_name' : 'Fahmi',
                            'email'     : 'farizfahmi674@gmail.com',
                            'phone'     : '081234631783'
                        }
                    }
                }
            },
            methods: {
                handlePayButton: function(event){
                    this.$http.post('/api/generate/', {data: this.data_midtrans}).then((response)=>{
                        console.log(response.data)
                        snap.pay(response.data.data.token)
                    }, response => {
                        console.log('error : ' + response)
                    })
                }
            },
        })
    </script>
</body>
</html>