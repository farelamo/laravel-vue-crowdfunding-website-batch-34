export default {
    namespaced: true,
    state: {
        transactions: 1
    },
    mutations: {
        insert: (state, payload) => {
            state.transactions++
        }
    },
    actions: {

    },
    getters: {
        transactions: state => state.transactions
    }
}