<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CampaignsController;
use App\Http\Controllers\BlogsController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Auth\LogoutController;
use App\Http\Controllers\Auth\checkTokenController;
use App\Http\Controllers\Auth\SocialiteController;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\MidtransController;

Route::group(['prefix' => 'auth'], function(){
    Route::post('/login', [LoginController::class, 'login']);
    Route::post('/register', [RegisterController::class, 'register']);
    Route::post('/logout', [LogoutController::class, 'logout'])->middleware('auth:api');
    Route::post('/check-token', [checkTokenController::class, 'checkToken'])->middleware('auth:api');

    Route::get('/social/{provider}', [SocialiteController::class, 'redirectToProvider']);
    Route::get('/social/{provider}/callback', [SocialiteController::class, 'handleProviderCallback']);
});

Route::group([
        'middleware' =>'api', //jan lupa ganti auth:api
        'prefix' =>'campaign',
    ],function(){
        Route::get('/', [CampaignsController::class, 'index']);
        Route::get('/{id}', [CampaignsController::class, 'detail']);
        Route::get('search/{keyword}', [CampaignsController::class, 'search']);
        Route::get('random/{count}', [CampaignsController::class, 'random']);
        Route::post('store', [CampaignsController::class, 'store']);
});

Route::group([
        'middleware' =>'api',
        'prefix' =>'blog',
    ],function(){
        Route::get('random/{count}', [BlogsController::class, 'random']);
        Route::post('store', [BlogsController::class, 'store']);
});

Route::post('/payment/store', [PaymentController::class, 'store']);

Route::post('/generate', [MidtransController::class, 'generate']);