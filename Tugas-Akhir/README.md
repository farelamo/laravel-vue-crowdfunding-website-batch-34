# Resource Tugas Akhir
- link Video dan Screenshot
    - https://drive.google.com/drive/folders/1w9GntEfEn1_z8ABJ3dxV8XilwBv6UD__?usp=sharing

# Pre-Requisite
- composer require laravel/ui
- php artisan ui vue
- npm install
- npm run dev

**if error, do this**
- npm install vue-loader@^15.9.7 (for vue)
- npm install bootstrap-loader (for bootstrap)
- then, `npm run dev` again

# Requirements

### 1. Composer Installation

Sign In dengan Social Media, Implementasi menggunakan (**Google**)

```
composer require laravel/socialite
```

Fitur [Payment Gateway](https://docs.midtrans.com/en/core-api/bank-transfer) dengan CoreAPI (**Server-Side**)

```
composer require guzzlehttp/guzzle:^7.0
```

Fitur [Payment Gateway](https://github.com/Midtrans/midtrans-php) dengan Snap JS (**Client-Side**)
```
composer require midtrans/midtrans-php
```

### 2. NPM Installation

```
npm install vue-router@2 vuetify vuex@3 --save vuex-persist
```

## Alternatif 
Clone Repo Tugas Akhir, lalu `composer install` dan `npm install` pada terminal

# Source Learning
- https://docs.midtrans.com/en/core-api/bank-transfer (Dokumentasi API)
- https://github.com/Midtrans/midtrans-php            (Midtrans Github)
- https://dashboard.sandbox.midtrans.com/transactions (Login Akun Midtrans)
- https://simulator.sandbox.midtrans.com/bni/va/index (Testing Pembayaran (Via BNI))
- https://docs.guzzlephp.org/en/stable/overview.html#installation (Guzzle)
