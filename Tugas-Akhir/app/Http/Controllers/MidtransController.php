<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Midtrans;

class MidtransController extends Controller
{
    public function generate(Request $request){
        \Midtrans\Config::$serverKey = config('app.midtrans.server_key');
        // Set sanitization on (default)
        \Midtrans\Config::$isSanitized = true;
        // Set 3DS transaction for credit card to true
        \Midtrans\Config::$is3ds = true;

        $midtrans_transaction = Midtrans\Snap::createTransaction($request->data);

        return response()->json([
            'response_code' => '00',
            'response_msg'  => 'success',
            'data'  => $midtrans_transaction
        ]);
    }
}
