<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Resources\authResource;

class LoginController extends Controller
{
    public function login(Request $request){
        request()->validate([
            'email' => 'required',
            'password' => 'required'
        ]);

        if(!$token = auth()->attempt($request->only('email', 'password'))){
            return response()->json([
                'response_code' => '01',
                'response_message' => 'E-mail / Password Salah ! !',
            ], 200);
        }

        // return response()->json(compact('token'));
        return response()->json([
            'response_code' => '00',
            'response_message' => 'user berhasil login',
            'data' => [
                'token' => $token,
                'user'  => auth()->user()
            ]
        ]);
    }
}
