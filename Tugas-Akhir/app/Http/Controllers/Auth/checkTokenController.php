<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class checkTokenController extends Controller
{
    public function checkToken(Request $request){
       return response()->json([
           'response_code' => '00',
           'response_message' => 'token valid',
           'data' => true
       ], 200);
    }
}
