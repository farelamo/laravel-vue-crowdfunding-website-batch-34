//Nomor 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"]

//Cara Lain
var caraLain = daftarHewan.sort()
for (let i = 0; i < caraLain.length; i++) {
    console.log(caraLain[i])
}

//Hard Way
// for (let index = 0; index < daftarHewan.length; index++) {
//     if (index == 4) {
//         daftarHewan.unshift(daftarHewan[index])
//         daftarHewan.splice(index + 1, 1)
//     }
// }
// for (let i = 0; i < daftarHewan.length; i++) {
//     if (i == 2) {
//         daftarHewan.push(daftarHewan[i])
//         daftarHewan.splice(2, 1)
//     }
// }
// console.log(daftarHewan)

//Nomor 2
var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
function introduce(data){
    return "Nama saya " + data.name +
           ", umur saya " + data.age + 
           ", alamat saya di " + data.address +
           ", dan saya punya hobby yaitu " + data.hobby 
}
console.log(introduce(data))

//Nomor 3
function hitung_huruf_vocal(kata){
    let vocal = 'aiueo'
    let jumlah = 0
    for (let i = 0; i < kata.length; i++) {
        if(vocal.indexOf(kata.toLowerCase()[i]) !== -1){
            jumlah += 1
        }
    }
    return jumlah
}
var hitung_1 = hitung_huruf_vocal("Muhammad")
var hitung_2 = hitung_huruf_vocal("Iqbal")
console.log(hitung_1 , hitung_2) // 3 2

//Nomor 4
function hitung(angka){
    return angka*2 - 2
}
console.log( hitung(0) ) // -2
console.log( hitung(1) ) // 0
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log( hitung(5) ) // 8