<?php

namespace App\Models;

use App\Traits\useUuid;
use Illuminate\Database\Eloquent\Model;

class otp_codes extends Model
{
    use Uuid;

    protected $fillable = ['otp', 'valid_until', 'user_id'];

    public function user(){
        return $this->belongsTo('App\Models\User');
    }
}
