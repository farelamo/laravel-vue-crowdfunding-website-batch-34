<?php

namespace App\Models;

use App\Models\roles;
use App\Traits\useUuid;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable, useUuid;

    protected $fillable = ['name', 'email', 'password', 'role_id'];
    protected $casts = ['email_verified_at' => 'datetime'];

    public function user(){
        return $this->hasOne('App\Models\otp_codes');
    }
    
    public function roles()
    {
        return $this->belongsTo('App\Models\roles');
    }

    public function isAdmin(){
        if($this->role_id === $this->get_role_admin()){
            return true;
        }
        return false;
    }

    public function get_role_admin(){
        $role = roles::where('name', 'admin')->first();
        return $role->id;
    }

    public function get_role_user(){
        $role = roles::where('name', 'user')->first();
        return $role->id;
    }

    public static function boot(){
        parent::boot();
        static::creating( function($model){
            $model->role_id = $model->get_role_user();
        });
    }
}
