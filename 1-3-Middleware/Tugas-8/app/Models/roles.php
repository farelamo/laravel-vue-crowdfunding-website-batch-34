<?php

namespace App\Models;

use App\Traits\useUuid;
use Illuminate\Database\Eloquent\Model;

class roles extends Model
{
    use useUuid;
    protected $fillable = ['name'];
    public function users(){
        return $this->hasMany('App\Models\User');
    }
}
