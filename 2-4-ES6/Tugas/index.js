//nomor 1
    const luas = (p,l)=> {
        return p*l
    }
    console.log(luas(4,3))

    const keliling = (p,l)=> {
        return 2*(p+l)
    }
    console.log(keliling(2,3))

//nomor 2
    const literal = (firstName, lastName)=>{
    return {
            firstName,
            lastName,
            fullName: function(){
                console.log(`${firstName} ${lastName}`)
            }
           }
    }
    
    //Driver Code
    literal("William", "Imoh").fullName()

//nomor 3
    const newObject = {
        firstName: "Muhammad",
        lastName: "Iqbal Mubarok",
        address: "Jalan Ranamanyar",
        hobby: "playing football"
    }
    const {firstName, lastName, address, hobby} = newObject
    console.log(firstName, lastName, address, hobby)

//nomor 4
    const west = ["Will", "Chris", "Sam", "Holly"]
    const east = ["Gill", "Brian", "Noel", "Maggie"]
    const combined = [...west, ...east]
    console.log(combined)

//nomor 5
    const planet = "earth" 
    const view = "glass" 
    var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit ${planet}`
    console.log(before)