// Beberapa fitur penting  u/ di pelajari sbg dasar vue js nanti

/* 1. Let dan Const */

    //var (js lama) nama variable yg sama AKAN mereplace nilai sebelumnya
    var x = 1
    if(x === 1) {
        var x = 2
        console.log(x) //output 2
    }
    console.log(x) //output 2

    //let (js baru) nama variable yg sama TIDAK AKAN mereplace nilai sebelumnya
    let y = 1
    if(y === 1){
        let y = 2
        console.log(y) //output 2
    }
    console.log(y) //output 1

/* 2. Arrow Functions */

    //js lama
    function testLama(){
        //isi function
    }
    console.log(testLama())

    //js baru
    const testBaru = () => {
        //isi function
    }
    console.log(testBaru())

/* 3. Default Parameters */

    //js baru
    const multiply = (a, b = 2)=>{
        return a*b
    }
    //panggil function
    console.log(multiply(5,3)) //output 15
    console.log(multiply(5)) //output 10

/* 4. Template Literals */

    //js baru
    const nama = 'John'
    const age = 20
    const kalimat = `nama saya adalah ${nama} dan berusia ${age}`
    console.log(kalimat) //output nama saya adalah John dan berusia 20

/* 5. Enhanced object literals */

    //js lama
    const fullName = 'John Doe'
    const john = {
        fullName: fullName
    }

    //js baru
    const namaku = 'arilasso'
    const aril = {namaku}

/* 6. Destructuring */

    //tanpa destructuring
    const noDestuc = ()=>{
        // array
        var numbers = [1,2,3]

        var numberOne = numbers[0]
        var numberTwo = numbers[1]
        var numberThree = numbers[2]

        console.log(numberOne)

        // object
        var studentName = {
            firstName: 'Peter',
            lastName: 'Parker'
        };
        
        const firstName = studentName.firstName;
        const lastName = studentName.lastName;

        console.log(firstName)
    }

    //dengan destructuring
    const withDestruc = ()=> {
        //array
        let number = [1,2,3]
        const [numberOne, numberTwo, numberThree] = number
        console.log(numberOne) //output 1

        //object
        var studentName = {
            firstName : 'Peter',
            lastName  : 'Parker'
        }
        const {firstName, lastName} = studentName
        console.log(firstName) //output Peter
    }

/* 7. Rest Parameters + Spread Operator */

    /* Rest Parameters */
 
    //first example
    let scores = ['98', '95', '93', '90', '87', '85']
    let [first, second, third, ...restOfScores] = scores;
    
    console.log(first) // 98
    console.log(second) // 95
    console.log(third) // 93
    console.log(restOfScores) // [90, 87, 85] 

    //second example 
    const filter = (...rest) =>{
        return rest.filter(el => el.text !== undefined)
    }

    console.log(filter(1, {text: "wonderful"}, "next"))

    /* spread operator */
    let array1 = ['one', 'two']
    let array2 = ['three', 'four']
    let array3 = ['five', 'six']
    
    // js lama
    var kombineJsLama = array1.concat(array2).concat(array3)
    console.log(kombineJsLama) // ['one', 'two', 'three', 'four', 'five', 'six']
    
    // js baru 
    let kombineJsBaru = [...array1, ...array2, ...array3]
    console.log(kombineJsBaru) // ['one', 'two', 'three', 'four', 'five', 'six']