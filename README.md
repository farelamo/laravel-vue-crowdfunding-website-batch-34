# Laravel Vue Crowdfunding  Website Batch 34

# Source Learning
 - https://stackoverflow.com/questions/46826908/laravel-composer-could-not-install-tymon-jwt-auth (compatible JWT for Laravel 8)
    - composer require tymon/jwt-auth --ignore-platform-reqs

 - https://jwt-auth.readthedocs.io/en/develop/laravel-installation/ (JWT Auth Laravel)
  - Just take 3 steps : 
    - composer require tymon/jwt-auth
    - php artisan vendor:publish --provider="Tymon\JWTAuth\Providers\LaravelServiceProvider"
    - php artisan jwt:secret